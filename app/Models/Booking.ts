import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, ManyToMany, manyToMany,  } from '@ioc:Adonis/Lucid/Orm'
import Field from './Field'
import User from './User'

/**
 * @swagger
 * definitions:
 *  Booking:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *      title:
 *        type: string
 *      play_date_start:
 *        type: datetime
 *      play_date_end:
 *        type: datetime
 *    required:
 *      - title
 *      - play_date_start
 *      - play_date_end
 */

export default class Booking extends BaseModel {
  public serializeExtras = true
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string

  @column()
  public playDateStart: DateTime

  @column()
  public playDateEnd: DateTime

  @column()
  public userId: number

  @column()
  public fieldId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(()=> Field)
  public field: BelongsTo<typeof Field>

  @belongsTo(()=> User)
  public bookingUser: BelongsTo<typeof User>

  @manyToMany(()=> User,{
    pivotTable: 'schedules'
  })
  public players: ManyToMany<typeof User>
  
}
