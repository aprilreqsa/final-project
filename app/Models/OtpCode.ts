import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import User from './User'


export default class OtpCode extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public otpCode: number

  @column()
  public userId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasOne(()=>User)
  public kodeOtp: HasOne<typeof User>
}
