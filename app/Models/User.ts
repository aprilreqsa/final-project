import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
  hasOne,
  HasOne,
} from '@ioc:Adonis/Lucid/Orm'
import Booking from './Booking'
import OtpCode from './OtpCode'
/**
 * @swagger
 * definitions:
 *  User:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *      name:
 *        type: string
 *      email:
 *        type: string
 *      password:
 *        type: string
 *      role:
 *        type: string
 *    required:
 *      - name
 *      - email
 *      - password
 *      - role
 *  login:
 *    type: object
 *    properties:
 *      email:
 *        type: string
 *      password:
 *        type: string
 *    required:
 *      - email
 *      - password
 *  otpCode:
 *    type: object
 *    properties:
 *      email:
 *        type: string
 *      otpCode:
 *        type: number
 *      
 */
 
export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public role: string

  @column()
  public isVerified: boolean

  @column()
  public rememberMeToken?: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }
  @hasMany(()=> Booking)
  public myBooking: HasMany<typeof Booking>

  @hasOne(()=>OtpCode)
  public kodeOtp: HasOne<typeof OtpCode>
}
