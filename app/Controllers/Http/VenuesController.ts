import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Venue from 'App/Models/Venue'
import VenueValidator from 'App/Validators/VenueValidator'

export default class VenuesController {
/**
 * @swagger
 * /api/v1/venues:
 *  post:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues
 *      summary: untuk menambah venue baru
 *      requestBody:
 *          required: true
 *          content:
 *              application/x-www-form-urlencoded:
 *                  schema:
 *                      $ref: '#definitions/Venue'
 *              application/json:
 *                  scheman:
 *                      $ref: '#definitions/Venue'
 *      responses:
 *          '201':
 *              description: success create venue
 *          '401':
 *              description: request invalid, hanya admin yang dapat masuk
 */   
    public async store({request, response}:HttpContextContract){
        try {
        const payload = await request.validate(VenueValidator)
        const name = payload.name
        const address = payload.address
        const phone = payload.phone
        const newVenue = await Venue.create({name,address,phone,})
        response.created({message: 'Created!', newID: newVenue})
        } catch (error) {
            response.badRequest({message: error.messages})
        }
    }
 /**
 * @swagger
 * /api/v1/venues:
 *  get:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues
 *      summary: untuk melihat venue beserta field
 *      responses:
 *          '201':
 *              description: success fetch venue
 *          '401':
 *              description: request invalid, hanya admin yang dapat masuk
 */   
    public async index({ response}:HttpContextContract){
        let venue = await Venue.query().preload('fields')
        return response.status(200).json({message: 'success',data: venue})    
    }
/**
 * @swagger
 * paths:
 *  /api/v1/venues/{venueId}:
 *   get:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues
 *      summary: untuk melihat venue berdasarkan idnya  
 *      parameters:
 *          - name: venueId
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '201':
 *            description: success fetch venue by id
 *          '200':
 *            description: data tidak ada
 *          '401':
 *            description: request invalid, hanya admin yang dapat masuk
 */   
    public async show({params, response}: HttpContextContract){
        let venue = await Venue.query().where('id', params.id).preload('fields').first()
        return response.status(200).json({message: 'success',data: venue})
    }
/**
 * @swagger
 * paths:
 *  /api/v1/venues/{venueId}:
 *   put:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues    
 *      summary: untuk update data venue
 *      parameters:
 *          - name: venueId
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      requestBody:
 *          required: true
 *          content:
 *              application/x-www-form-urlencoded:
 *                  schema:
 *                      $ref: '#definitions/Venue'
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/Venue'
 *      responses:
 *          '202':
 *            description: success update
 *          '401':
 *            description: request invalid, hanya admin yang dapat masuk
 */   
    public async update({params,request,response}:HttpContextContract){
        let venue = await Venue.findOrFail(params.id)
        venue.name = request.input('name')
        venue.address = request.input('address')
        venue.phone = request.input('phone')
        venue.save()
        return response.ok({message: 'updated!'})
    }
    
}
