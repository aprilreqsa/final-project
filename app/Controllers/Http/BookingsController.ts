import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import FormBookingValidator from 'App/Validators/FormBookingValidator'

export default class BookingsController {
 /**
 * @swagger
 * /api/v1/bookings:
 *  get:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      summary: untuk melihat data booking
 *      responses:
 *          '200':
 *              description: success fetch booking
 *          '401':
 *              description: request invalid, please login or create user
 */   
    public async index({response}: HttpContextContract){
        const booking = await Booking.all()
        response.status(200).json({message: 'success', data: booking })
    }
/**
 * @swagger
 * paths:
 *  /api/v1/bookings/{bookingId}:
 *   get:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings  
 *      summary: untuk melihat data booking berdasarkan id
 *      parameters:
 *          - name: bookingId
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '200':
 *            description: success fetch bookings
 *          '401':
 *            description: request invalid, please login or create user
 */
    public async show({params, response}:HttpContextContract){
        const booking = await Booking.query().where('id',params.id).preload('players', (userQuery)=>{
            userQuery.select(['name','email','id'])
        }).withCount('players').firstOrFail()
        // let dataRaw = JSON.stringify(booking)
        // let dataWithCount = {  ...JSON.parse(dataRaw),player_count: booking.$extras.players_count}
        return response.status(200).json({message: 'success', data: booking})
    }
/**
 * @swagger
 * paths:
 *  /api/v1/fields/{fieldId}/bookings:
 *   post:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues    
 *      summary: untuk membooking field saat login
 *      parameters:
 *          - name: fieldId
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      requestBody:
 *          required: true
 *          content:
 *              application/x-www-form-urlencoded:
 *                  schema:
 *                      $ref: '#definitions/Booking'
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/Booking'
 *      responses:
 *          '201':
 *            description: success booking
 *          '401':
 *            description: request invalid, please login or create user
 */   
    public async store ({request,response,params,auth}:HttpContextContract){
            const field = await Field.findByOrFail('id', params.field_id)

            const user = auth.user!      

            await request.validate(FormBookingValidator)

            const booking = await new Booking()
            booking.title = request.input('title')
            booking.playDateStart = request.input('play_date_start')
            booking.playDateEnd = request.input('play_date_end')

            booking.related('field').associate(field)
            user.related('myBooking').save(booking)
            return response.created({message: 'created!', data: booking})
        
    }
/**
 * @swagger
 * paths:
 *  /api/v1/bookings/{bookingId}:
 *   put:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings  
 *      summary: untuk join/unjoin jadwal
 *      parameters:
 *          - name: bookingId
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '200':
 *            description: success join/unjoin
 *          '401':
 *            description: request invalid, please login or create user
 */
    public async join({response,auth, params}:HttpContextContract){
        const booking = await Booking.findOrFail(params.id)
        let user = auth.user!
        const checkJoin = await Database.from('schedules').where('booking_id', params.id).where('user_id', user.id).first()
        if (!checkJoin){
            await booking.related('players').attach([user.id])
        }else{
            await booking.related('players').detach([user.id])
        }
        

        return response.ok({status: 'success', data: 'successfully join/unjoin'})
    }
/**
 * @swagger
 * /api/v1/schedules:
 *  get:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      summary: melihat jadwal yang sedang berlangsung
 *      responses:
 *          '200':
 *              description: success fetch schedules
 *          '401':
 *              description: request invalid, please login or create user
 */   
    public async schedules({response,auth}:HttpContextContract){
        let user = auth.user!
        const jadwal = await Database.from('schedules').where('user_id',user.id).firstOrFail()
        return response.ok({message: 'success', data: jadwal})
    }
    
}
