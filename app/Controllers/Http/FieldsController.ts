import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

export default class FieldsController {
/**
 * @swagger
 * paths:
 *  /api/v1/venues/{venueId}/fields:
 *   post:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues 
 *      summary: menambah kan field berdasarkan venue id
 *      parameters:
 *          - name: venueId
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      requestBody:
 *          required: true
 *          content:
 *              application/x-www-form-urlencoded:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          name:
 *                              type: string
 *                          type:
 *                              type: enum[soccer|minisoccer|futsal|basketball|volleyball]
 *                      required:
 *                          - name
 *                          - type  
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          name:
 *                              type: string
 *                          type:
 *                              type: enum[soccer|minisoccer|futsal|basketball|volleyball]
 *                      required:
 *                          - name
 *                          - type 
 *      responses:
 *          '200':
 *            description: success created field
 *          '401':
 *            description: request invalid, hanya admin yang boleh masuk
 */
    public async store({request,response,params}: HttpContextContract){
        const venue = await Venue.findByOrFail('id',params.venue_id)
        const newField = await new Field()
        newField.name = request.input('name'),
        newField.type = request.input('type')
        await newField.related('venue').associate(venue)

        return response.created({message: 'success', data: newField})
    }
    public async show({response,params}:HttpContextContract){
        const field = await Field.query().where('id', params.id).preload('bookings',(bookingQuery)=>{
            bookingQuery.select('title','play_date_start','play_date_end')
        }).firstOrFail()
        response.status(200).json({message: 'success', data: field})
    }

}
