import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import { schema }from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import Mail from '@ioc:Adonis/Addons/Mail'
import UserValidator from 'App/Validators/UserValidator'

export default class AuthController {
/**
 * @swagger
 * /api/v1/register:
 *  post:
 *      tags:
 *          - Authentication
 *      summary: Register untuk Daftar
 *      requestBody:
 *          required: true
 *          content:
 *              application/x-www-form-urlencoded:
 *                  schema:
 *                      $ref: '#definitions/User'
 *              application/json:
 *                  scheman:
 *                      $ref: '#definitions/User'
 *      responses:
 *          '201':
 *              description: user created, verify otp in email
 *          '422':
 *              description: request invalid
 */
    
    public async register({request, response}: HttpContextContract){
            const payload = await request.validate(UserValidator)
            const name = payload.name
            const email = payload.email
            const password = payload.password
            const role = payload.role
            const newUser = await User.create({name,email,password,role})

            const otp_code = Math.floor(100000+ Math.random()*900000)
            await Database.table('otp_codes').insert({otp_code: otp_code, user_id: newUser.id})
            await Mail.send((message)=>{
                message
                .from('adonis.demo@sanberdev.com')
                .to(email)
                .subject('Welcome On Board')
                .htmlView('emails/otp_verification',{otp_code: otp_code})
            })
            return response.created({message: 'register success please verify your otp code '})
        }
/**
 * @swagger
 * /api/v1/login:
 *  post:
 *      tags:
 *          - Authentication
 *      summary: Login untuk masuk
 *      requestBody:
 *          required: true
 *          content:
 *              application/x-www-form-urlencoded:
 *                  schema:
 *                      $ref: '#definitions/login'
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/login'
 *      responses:
 *          '200':
 *              description: login sukses
 *          '400':
 *              description: request invalid
 */
    public async login({request, response,auth}: HttpContextContract){
        const userSchema = schema.create({
            email: schema.string(),
            password: schema.string()
        })
        try {
            const payload = await request.validate({schema: userSchema})
            const token = await auth.use('api').attempt(payload.email, payload.password)
            return response.ok({message: 'login sukses', token})
        } catch (error) {
            return response.badRequest({message: error.messages})
            
        }
    }
/**
 * @swagger
 * /api/v1/otp-verification:
 *  post:
 *      tags:
 *          - Authentication
 *      summary: OTP verification
 *      requestBody:
 *          required: true
 *          content:
 *              application/x-www-form-urlencoded:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          otp_code:
 *                              type: number
 *                          email:
 *                              type: string
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          otp_code:
 *                              type: number
 *                          email:
 *                              type: string
 *      responses:
 *          '200':
 *              description: berhasil konfirmasi OTP
 *          '500':
 *              description: gagal verifikasi OTP
 */    
    public async otpConfirmation({request, response}:HttpContextContract){
        let otp_code = request.input('otp_code')
        let email =  request.input('email')

        let user = await User.findByOrFail('email', email)

        let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).firstOrFail()
        if (user.id == otpCheck.user_id){  
        user.isVerified = true
        await user.save()
        return response.status(200).json({message: 'berhasil konfirmasi OTP',data: 'verification succeeded'})
        }else{
            return response.status(400).json({message: 'gagal verifikasi OTP'})
        }
    }
}
