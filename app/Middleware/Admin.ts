import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Admin {
  public async handle ({response,auth}: HttpContextContract, next: () => Promise<void>) {
    let admin = auth.user?.role
    if (admin == 'owner'){
      await next()
    }else{
      return response.unauthorized({message: 'hanya admin yang dapat masuk'})
    }
}
}
